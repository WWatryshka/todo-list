let add = document.querySelector('.add-button');
let inp = document.querySelector('.content');


add.addEventListener('click',function() {
    if(inp.value.length > 0) {  
        var newDiv = document.createElement("div");
        var newDel = document.createElement("button");
        var newCont = document.createElement("div");
        var newEdit = document.createElement("button");
        newDel.className = "del";
        newEdit.className = "edit";
        newDiv.className = "newToDo";
        newCont.className = "td";
        newCont.innerHTML = inp.value;
        newDel.innerHTML = "X";
        newEdit.innerHTML = "Edit";
        var container = document.querySelector('.content-block');
        container.appendChild(newDiv);
        newDiv.appendChild(newCont);
        newDiv.appendChild(newDel);   
        newDiv.appendChild(newEdit);
        inp.value = '';
        var ed = document.querySelectorAll('.edit');
        var tdn = document.querySelectorAll('.newToDo');
        var del = document.querySelectorAll('.del');
        for(let e = 0; e < ed.length; e++){
            ed[e].onclick = function () {
                newCont.contentEditable = true;
                var newOk = document.createElement("button");
                newOk.innerHTML = 'OK';
                newDiv.appendChild(newOk);
                newOk.addEventListener('click',function(){
                    newCont.contentEditable = false;
                })
            }
        }
        for(let i = 0; i < del.length;i++){
            del[i].onclick = function (){
                tdn[i].remove();
            }
        
    }
  
    
   
    }})